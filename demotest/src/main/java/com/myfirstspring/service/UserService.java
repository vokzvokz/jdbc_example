package com.myfirstspring.service;

import com.myfirstspring.entity.User;

import java.util.List;

public interface UserService {
    List<User> fidAll();

    void save(User user);

    User getById(int id);

    void delete(int id);

    void update(User user);
}
